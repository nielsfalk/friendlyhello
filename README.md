# Objective
Take the Docker Getting Started example webapp, build it using Gitlab CI and
host it on Gitlab's Registry.

## Write the app
- Commit the Dockerfile, app.py and requirements.txt from https://docs.docker.com/get-started/part2/
- Test that it is buildable using "docker build -t friendlyhello ."

## Gitlab CI setup
- Create a Gitlab API key for command line use. Only **required** if you have
  2FA enabled, but without it, your gitlab password is going to be in plain text
  in a CI variable. https://gitlab.com/profile/personal_access_tokens
- TODO: Does it need to be a full api level token, or can you push with a lower
  one?
- Create a secret variable containing the token you just made.(Settings -> CI/CD
  called GITLAB_API_KEY). This way the runner has access to push to your
  registry

## Trigger the run
- Commit a .gitlab-ci.yml file using the following as guides
  - https://about.gitlab.com/2016/05/23/gitlab-container-registry/ 
  - https://docs.gitlab.com/ce/ci/variables/README.html
- git push
- Watch the build happening https://gitlab.com/plett/friendlyhello/-/jobs
- Hope it passes

## Use the image
- docker login registry.gitlab.com
- docker pull registry.gitlab.com/plett/friendlyhello
- docker run -p 4000:80 registry.gitlab.com/plett/friendlyhello
- visit http://localhost:4000/
